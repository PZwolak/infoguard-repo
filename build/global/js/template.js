'use strict';

// file: detect-browser.js
$(document).ready(function () {
    iOS();
    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
        $('body').addClass('ms-browser');
    }

    if (!!window.MSInputMethodContext && !!document.documentMode) {
        $('body').addClass('ie');
    }
});

function iOS() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) $('body').addClass('ios');
}

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    $('body').addClass('ms-browser');
}

if (!!window.MSInputMethodContext && !!document.documentMode) {
    $('body').addClass('ie');
}

if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
    $('body').addClass('ff');
}

if (navigator.userAgent.toLowerCase().indexOf('safari/') > -1 && navigator.userAgent.toLowerCase().indexOf('chrome') < 1) {
    $('body').addClass('safari');
}

if ("ontouchstart" in document.documentElement) {
    $('body').addClass('touch-device');
}

// end file: detect-browser.js

// file: Landing/infoGuard_New Advent Calendar Full.js
// $(document).ready(function () {

//     if (document.body.classList.contains('advent-calendar')) {
//         const adventCalendarLogoUrl = document.querySelector('.advent-calendar .header-container-wrapper #hs-link-logo');
//         adventCalendarLogoUrl.setAttribute("href", "https://www.infoguard.ch/de-ch/de")
//     }

// })
// end file: Landing/infoGuard_New Advent Calendar Full.js

// file: Modules/accordion-new.js
$(document).ready(function () {

    var accordion = $('.accordion-new-wrapper');
    accordion.find('.accordion-description').hide();

    accordion.find('.accordion-element.active .accordion-description').show();
    accordion.find('.accordion-element.active .accordion-header .standard-text').toggleClass('active');

    accordion.find('.accordion-element').on('click', function () {
        $(this).find('.accordion-description').toggle(300);
        $(this).find('.accordion-header .standard-text').toggleClass('active');
    });
});
// end file: Modules/accordion-new.js

// file: Modules/cta-popup.js
// scripts in module cta-popup
// end file: Modules/cta-popup.js

// file: Modules/download-image-slider.js
$('.download-image-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: false,
    responsive: [{
        breakpoint: 992,
        settings: {
            slidesToShow: 2
        }
    }, {
        breakpoint: 576,
        settings: {
            slidesToShow: 1
        }
    }]
});
// end file: Modules/download-image-slider.js

// file: Modules/iq-quiz-wrapper.js
$(document).ready(function () {
    //initializing quiz container
    var quiz = $('.ig-quiz-wrapper'),
        answerIndex = 0,
        answerType,
        answerAmount,
        answerSummary,
        points = 0,
        flag = true,
        ts,
        timeout = 400;

    //main Function
    var correctIncorrect = function correctIncorrect(aT, ts) {
        //hide previous answer
        setTimeout(function () {
            $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\'] .show-simple-answer').fadeIn(400);
        }, 400);

        //if answer is true
        if (aT == "true") {
            points++;
            $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\'] .answers .answer[data-answer-type="true"]').addClass('green');
            setTimeout(function () {}, 1000);
            setTimeout(function () {
                $(quiz).removeClass('correct');
                $(quiz).find('.ig-quiz-element.correct').fadeOut(400);
            }, 1500);
        }
        //if answer is wrong
        else {
                $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\'] .answers .answer[data-answer-type="true"]').addClass('green');
                $(ts).addClass('red');
                setTimeout(function () {}, 1000);
                setTimeout(function () {
                    $(quiz).removeClass('incorrect');
                    $(quiz).find('.ig-quiz-element.incorrect').fadeOut(400);
                }, 1500);
            }
    };

    var showSimpleAnswer = function showSimpleAnswer() {
        $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\']').fadeOut(400, function () {
            $(quiz).find('.simple-text-after-answer[data-index-type=\'' + answerIndex + '\']').fadeIn(400);
        });
    };

    var backToQuestions = function backToQuestions() {
        $(quiz).find('.simple-text-after-answer[data-index-type=\'' + answerIndex + '\']').fadeOut(400, function () {
            $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\']').fadeIn(400);
        });
    };

    var nextQuestion = function nextQuestion() {
        $(quiz).find('.simple-text-after-answer[data-index-type=\'' + answerIndex + '\']').fadeOut(400);
        answerIndex++;
        setTimeout(function () {
            $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\']').fadeIn(400);
        }, 400);

        if (answerIndex == answerAmount + 1) {
            answerSummary = Math.round(points / answerAmount * 100);
            setTimeout(function () {
                if (answerSummary <= 40) {
                    $(quiz).find('.ig-quiz .quiz-result.to-40 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-40 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-40').fadeIn(400);
                } else if (answerSummary >= 41 && answerSummary <= 70) {
                    $(quiz).find('.ig-quiz .quiz-result.to-70 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-70 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-70').fadeIn(400);
                } else {
                    $(quiz).find('.ig-quiz .quiz-result.to-100 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-100 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-100').fadeIn(400);
                }
            }, 400);
        }
    };

    var previousQuestion = function previousQuestion() {
        $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\']').fadeOut(400);
        answerIndex--;
        setTimeout(function () {
            $(quiz).find('.simple-text-after-answer[data-index-type=\'' + answerIndex + '\']').fadeIn(400);
        }, 400);
    };

    if (quiz.length > 0) {

        //initial settings
        $(quiz).find('.ig-quiz .ig-quiz-element').hide();
        $(quiz).find('.ig-quiz .quiz-start').show();
        answerAmount = $(quiz).find('.quiz-question').length;

        // click on start quiz
        $(quiz).find('.ig-quiz .quiz-start .start-quiz-button').on('click', function () {
            answerIndex++;
            $(quiz).find('.ig-quiz .quiz-start').fadeOut(400);
            setTimeout(function () {
                $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\']').fadeIn(400);
            }, 400);
        });

        //click on answer
        $(quiz).find('.ig-quiz .ig-quiz-element .answers .answer').on('click', function () {
            if (flag === true) {
                $(quiz).find('.quiz-question[data-index-type=\'' + answerIndex + '\'] .answers .answer').addClass('disabled');
                answerType = $(this).attr("data-answer-type");
                ts = $(this);
                correctIncorrect(answerType, ts);
                flag = false; //set flag for only one click
            }
            //set flag for only one click
            setTimeout(function () {
                flag = true;
            }, 2000);
        });

        // click on the arrow button in quiz questions to move to quiz simple answer
        $(quiz).find('.ig-quiz .show-simple-answer--next button').on('click', function () {
            showSimpleAnswer();
        });

        // click on the arrow button in quiz questions to move to previous question
        $(quiz).find('.ig-quiz .show-simple-answer--prev button').on('click', function () {
            previousQuestion();
        });

        // click on the arrow button in quiz simple answer to move to next question
        $(quiz).find('.ig-quiz .next-question--next button').on('click', function () {
            nextQuestion();
        });

        // click on the arrow button in quiz questions to move to back to questions
        $(quiz).find('.ig-quiz .next-question--prev button').on('click', function () {
            backToQuestions();
        });

        //click on again button in results section
        $(quiz).find('.ig-quiz .quiz-result .result-buttons .again-quiz-button').on('click', function () {
            $(quiz).find('.ig-quiz .show-simple-answer').hide();
            $(quiz).find('.ig-quiz .ig-quiz-element').fadeOut(400);
            $(quiz).find('.quiz-question .answers .answer').removeClass('green').removeClass('red').removeClass('disabled');
            setTimeout(function () {
                answerIndex = 0, points = 0;
                $(quiz).find('.ig-quiz .quiz-start').fadeIn(400);
            }, 400);
        });
    };
});
// end file: Modules/iq-quiz-wrapper.js

// file: Modules/scroll-back-button.js
$(document).ready(function () {

    if (!$('body').hasClass('cybeer')) {
        var backButton = $('.scroll-back-button');
        var debounce_timer = void 0;
        if (backButton.length > 0) {
            backButton.on('click', function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 100);
            });
            $(document).on('scroll', function () {
                if (debounce_timer) {
                    window.clearTimeout(debounce_timer);
                }
                debounce_timer = window.setTimeout(function () {
                    if ($(window).height() / 2 < $(window).scrollTop()) {
                        backButton.animate({
                            right: '20px'
                        }, 300);
                    } else {
                        backButton.animate({
                            right: '-100px'
                        }, 300);
                    }
                }, 30);
            });
        }
    }
});
// end file: Modules/scroll-back-button.js

// file: Modules/test.js
// $(document).ready(function () {
//     $(window).on('click', function (e) {
//         console.log(e.target);
//     })
// })
// end file: Modules/test.js

// file: Modules/text-slider.js
$('.text-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
});
// end file: Modules/text-slider.js
//# sourceMappingURL=template.js.map
