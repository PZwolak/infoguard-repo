$(document).ready(function () {
	iOS();
	if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
		$('body').addClass('ms-browser');
	}

	if(!!window.MSInputMethodContext && !!document.documentMode){
		$('body').addClass('ie');
	}
});

function iOS() {
	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	if (iOS) $('body').addClass('ios');
}

if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) ||/Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1 ){
	$('body').addClass('ms-browser');
}

if(!!window.MSInputMethodContext && !!document.documentMode){
	$('body').addClass('ie');
}

if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
	$('body').addClass('ff');
}

if(navigator.userAgent.toLowerCase().indexOf('safari/') > -1 && navigator.userAgent.toLowerCase().indexOf('chrome') < 1){
	$('body').addClass('safari');
}

if ("ontouchstart" in document.documentElement) {
	$('body').addClass('touch-device');
}
