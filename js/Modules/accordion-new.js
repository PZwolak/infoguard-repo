$(document).ready(function () {

    var accordion = $('.accordion-new-wrapper');
    accordion.find('.accordion-description').hide();

     accordion.find('.accordion-element.active .accordion-description').show();
     accordion.find('.accordion-element.active .accordion-header .standard-text').toggleClass('active');

    accordion.find('.accordion-element').on('click', function () {
        $(this).find('.accordion-description').toggle(300);
        $(this).find('.accordion-header .standard-text').toggleClass('active');
    });

});