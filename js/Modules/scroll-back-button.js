$(document).ready(function () {

    if (!$('body').hasClass('cybeer')) {
        const backButton = $('.scroll-back-button');
        let debounce_timer;
        if (backButton.length > 0) {
            backButton.on('click', function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 100);
            });
            $(document).on('scroll', function () {
                if (debounce_timer) {
                    window.clearTimeout(debounce_timer);
                }
                debounce_timer = window.setTimeout(function () {
                    if ($(window).height() / 2 < $(window).scrollTop()) {
                        backButton.animate({
                            right: '20px'
                        }, 300)
                    } else {
                        backButton.animate({
                            right: '-100px'
                        }, 300)
                    }
                }, 30);
            })
        }
    }
});