$(document).ready(function () {
    //initializing quiz container
    var quiz = $('.ig-quiz-wrapper'),
    answerIndex = 0,
    answerType, answerAmount, answerSummary, points = 0, flag=true,
    ts, timeout = 400;

    //main Function
    var correctIncorrect = function (aT, ts) {
        //hide previous answer
        setTimeout(() => {
            $(quiz).find(`.quiz-question[data-index-type='${answerIndex}'] .show-simple-answer`).fadeIn(400);
        }, 400);

        //if answer is true
        if (aT == "true") {
            points++;
            $(quiz).find(`.quiz-question[data-index-type='${answerIndex}'] .answers .answer[data-answer-type="true"]`).addClass('green');
            setTimeout(function () {

            }, 1000);
            setTimeout(function () {
                $(quiz).removeClass('correct');
                $(quiz).find('.ig-quiz-element.correct').fadeOut(400);
            }, 1500);
        }
        //if answer is wrong
        else {
            $(quiz).find(`.quiz-question[data-index-type='${answerIndex}'] .answers .answer[data-answer-type="true"]`).addClass('green');
            $(ts).addClass('red');
            setTimeout(function () {

            }, 1000);
            setTimeout(function () {
                $(quiz).removeClass('incorrect');
                $(quiz).find('.ig-quiz-element.incorrect').fadeOut(400);
            }, 1500);
        }

    }


    var showSimpleAnswer = () => {
        $(quiz).find(`.quiz-question[data-index-type='${answerIndex}']`).fadeOut(400, () => {
            $(quiz).find(`.simple-text-after-answer[data-index-type='${answerIndex}']`).fadeIn(400);
        });
    }

    var backToQuestions = () => {
        $(quiz).find(`.simple-text-after-answer[data-index-type='${answerIndex}']`).fadeOut(400, () => {
            $(quiz).find(`.quiz-question[data-index-type='${answerIndex}']`).fadeIn(400);
        });
    }


    var nextQuestion = function () {
        $(quiz).find(`.simple-text-after-answer[data-index-type='${answerIndex}']`).fadeOut(400);
        answerIndex++;
        setTimeout(() => {
            $(quiz).find(`.quiz-question[data-index-type='${answerIndex}']`).fadeIn(400);
        }, 400);

        if (answerIndex == answerAmount + 1) {
            answerSummary = Math.round(points / answerAmount * 100);
            setTimeout(function () {
                if (answerSummary <= 40) {
                    $(quiz).find('.ig-quiz .quiz-result.to-40 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-40 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-40').fadeIn(400);
                } else if (answerSummary >= 41 && answerSummary <= 70) {
                    $(quiz).find('.ig-quiz .quiz-result.to-70 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-70 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-70').fadeIn(400);
                } else {
                    $(quiz).find('.ig-quiz .quiz-result.to-100 .result-header .title .points').text(points);
                    $(quiz).find('.ig-quiz .quiz-result.to-100 .result-header .title .all-answers').text(answerAmount);
                    $(quiz).find('.ig-quiz .quiz-result.to-100').fadeIn(400);
                }
            }, 400);
        }

    }

    var previousQuestion = function () {
        $(quiz).find(`.quiz-question[data-index-type='${answerIndex}']`).fadeOut(400);
        answerIndex--;
        setTimeout(() => {
            $(quiz).find(`.simple-text-after-answer[data-index-type='${answerIndex}']`).fadeIn(400);
        }, 400);
    }


    if (quiz.length > 0) {

        //initial settings
        $(quiz).find('.ig-quiz .ig-quiz-element').hide();
        $(quiz).find('.ig-quiz .quiz-start').show();
        answerAmount = $(quiz).find('.quiz-question').length;

        // click on start quiz
        $(quiz).find('.ig-quiz .quiz-start .start-quiz-button').on('click', function () {
            answerIndex++;
            $(quiz).find('.ig-quiz .quiz-start').fadeOut(400);
            setTimeout(function () {
                $(quiz).find(`.quiz-question[data-index-type='${answerIndex}']`).fadeIn(400);
            }, 400);
        });

        //click on answer
        $(quiz).find('.ig-quiz .ig-quiz-element .answers .answer').on('click', function () {
            if(flag===true){
                $(quiz).find(`.quiz-question[data-index-type='${answerIndex}'] .answers .answer`).addClass('disabled');
                answerType = $(this).attr("data-answer-type");
                ts = $(this);
                correctIncorrect(answerType, ts);
                    flag=false;//set flag for only one click
                }
                //set flag for only one click
                setTimeout(() => {
                    flag=true; 
                }, 2000);
            });

        // click on the arrow button in quiz questions to move to quiz simple answer
        $(quiz).find('.ig-quiz .show-simple-answer--next button').on('click', function () {
            showSimpleAnswer();
        });

        // click on the arrow button in quiz questions to move to previous question
        $(quiz).find('.ig-quiz .show-simple-answer--prev button').on('click', function () {
            previousQuestion();
        });

        // click on the arrow button in quiz simple answer to move to next question
        $(quiz).find('.ig-quiz .next-question--next button').on('click', function () {
            nextQuestion();
        });

        // click on the arrow button in quiz questions to move to back to questions
        $(quiz).find('.ig-quiz .next-question--prev button').on('click', function () {
            backToQuestions();
        });


        //click on again button in results section
        $(quiz).find('.ig-quiz .quiz-result .result-buttons .again-quiz-button').on('click', function () {
            $(quiz).find('.ig-quiz .show-simple-answer').hide();
            $(quiz).find('.ig-quiz .ig-quiz-element').fadeOut(400);
            $(quiz).find(`.quiz-question .answers .answer`).removeClass('green').removeClass('red').removeClass('disabled');
            setTimeout(function () {
                answerIndex = 0, points = 0;
                $(quiz).find('.ig-quiz .quiz-start').fadeIn(400);
            }, 400);
        })
    };
});